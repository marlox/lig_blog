# LIG BLOG

A simple application for content creation with image attachment. This will also serve as a tool to publish news and/or product-related reviews.


## Features

### 1. Admin
* Add Articles
* Edit Articles

### 2. Guests
* Read Articles
* Download Image attachment (low or high-resolution)


## Specifications
* Ruby Version: 2.4.x
* Rails Version: 5.1.4
* Database: PostgreSQL


## Demo
* [LIG Blog](https://lig-blog.herokuapp.com)
* [Admin Page](https://lig-blog.herokuapp.com/admins)
* Credentials:
  * Email: admin@ligblog.com
  * Password: 8nFNV3
