Rails.application.routes.draw do

  root to: 'articles#index'

  get "/articles/archive" => "articles#archive", as: :archive

  resources :articles, only: [:index, :show]

  devise_for :admins, skip: :registrations

  scope :admins do
    get "/" => "articles#list", as: :admin_root
    get "/articles" => "articles#list", as: :admin_articles
    resources :articles, only: [:create, :update], as: :admin_article
    resources :articles, only: [:new, :edit]
  end

end
