class CreateArticles < ActiveRecord::Migration[5.1]
  def change
    create_table :articles do |t|
      t.string      :title, null:false
      t.text        :contents, null:false
      t.attachment  :eye_catch

      t.timestamps  null:false
    end
  end
end
