module ApplicationHelper

  def is_admin?
    request.url["/admins"].present?
  end

  def not_found
    raise ActionController::RoutingError.new("Not Found")
  end

  def render_model_success model, action
    flash_color = case action.to_sym
      when :create
        "success"
      when :delete
        "alert"
      else
        "info"
      end
    flash[flash_color] = t("activerecord.flash.successful_#{action}",  resource: model.class.model_name.human)
  end

  def render_model_errors model
    model_class = model.class
    flash[:alert] = model.errors.map do |key, values|
      "<li><strong>#{model_class.human_attribute_name(key)}</strong> #{values}</li>"
    end.flatten.join.prepend("<ul>").concat("</ul>")
      .prepend(t("activerecord.flash.error_intro", resource: model.class.model_name.human))
  end

  def class_for flash_type
    { success: "success", error:"alert", warning: "warning", alert: "alert",
      notice: "info", notice_default:"secondary"}[flash_type.to_sym] || flash_type.to_s
  end

  def icon_for flash_type
    { success: "check", error: "alert", danger: "stop", alert: "alert",
     notice: "info",notice_default: "info"}[flash_type.to_sym] || "info"
  end

  def flash_messages opts={}
    render partial: "application/flash_messages", locals: { opts: opts }
  end

  def pagination_page_indicator smart_list, container_class, opts={}
    render partial: "smart_listing/pagination_page_custom", locals: { smart_listing: smart_list, container_classes: container_class, opts: opts }
  end

end
