class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_admin!

  include SmartListing::Helper::ControllerExtensions
  helper SmartListing::Helper
  include ApplicationHelper

  protected
    def authenticate_admin!
      return unless is_admin?

      super
    end
end
