class ArticlesController < ApplicationController

  before_action :get_articles
  before_action :get_article, only: [:show, :edit, :update]

  def index
    smart_listing_create :articles, @articles, partial: "articles/articles_archive",
      default_sort: { id: "desc" }, page_sizes: [5]
  end

  def show
  end

  def archive
    smart_listing_create :articles, @articles, partial: "articles/articles_archive",
      default_sort: { id: "desc" }, page_sizes: [5, 10, 15], remote: false
  end

  def list
    smart_listing_create :articles, @articles, partial: "articles/articles_list",
      default_sort: { id: "desc" }, page_sizes: [5, 10, 15], remote: false
  end

  def new
    @article = @articles.new
  end

  def create
    @article = @articles.create article_params

    if @article.save
      render_model_success @article, "create"
      redirect_to edit_article_path(@article)
    else
      render_model_errors @article
      render :new
    end
  end

  def edit
  end

  def update
    if @article.update article_params
      render_model_success @article, "update"
      redirect_to edit_article_path(@article)
    else
      render_model_errors @article
      render :edit
    end
  end

  private

    def article_params
      permitted = params.require(:article).permit(:eye_catch, :title, :contents)
      permitted.each do |key,val|
        permitted[key.to_sym] = ApplicationController.helpers.strip_tags(val) unless key.to_sym == :eye_catch
      end
      permitted
    end

    def get_articles
      @articles = Article.all
    end

    def get_article
      @article = @articles.find_by id: params[:id]

      not_found if @article.blank?
    end

end
