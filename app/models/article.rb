class Article < ApplicationRecord

  has_attached_file :eye_catch,
                    :styles => { :large => "1280x720>", :medium => "854x480>", :small => "640x360>", :thumb => "426x240>" },
                    :size => { :less_than => 2.megabyte }

  validates :title, presence: true, uniqueness: true
  validates :contents, presence: true
  validates_attachment :eye_catch, presence: true,
                      content_type: { content_type: /\Aimage\/.*\z/ },
                      name: { presence: true }, size: { greater_than: 0 }

  def eye_catch_safe_path style=nil
    path = style ? eye_catch.url(style.to_sym) : eye_catch.url
    File.exist?(path[1..-1]) ? path[1..-1] : path
  end

end
