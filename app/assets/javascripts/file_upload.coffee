$(document).on 'turbolinks:load', ->

  $('#article_eye_catch').on 'click', ->
    $('#form-filename').text('')

  $('#article_eye_catch').on 'change', ->
    $('#form-filename').text(this.files[0].name)

  $('.form-file.u-clear').on 'click', ->
    $('#article_eye_catch').trigger('click')
